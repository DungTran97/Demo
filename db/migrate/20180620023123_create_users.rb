class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :Name, null:false
      t.string :password_digest, null:false
      t.string :Email
      t.string :Facebook
      t.string :Telephone
      t.integer :CashIn
      t.integer :CashOut

      t.timestamps
    end
  end
end
