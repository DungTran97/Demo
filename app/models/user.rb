class User < ApplicationRecord
    has_secure_password
    validates_presence_of :Name, :password_digest
    validates_uniqueness_of :Name
end
