class Api::V1::UserController < ApplicationController
    def show
        render json: User.find(params[:id])
    end

    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user, status: 201 #Created
        else
            render json: {errors: user.errors}, status: 422 #Unprocessable Entity
        end
    end

    def update
        @user = User.find(params[:id])
        if @user.update(user_params)
            render json: @user, status: 200 #Request success
        else
            render json: {errors: user.errors}, status: 422
        end
    end

    private
    def user_params
        params.require(:user).permit(:Name,:password,:password_confirmation,:Email,:Facebook,:Telephone)
    end

    skip_before_action :verify_authenticity_token
    respond_to :json
end
