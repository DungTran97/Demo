class Api::V1::SessionController < ApplicationController
    def create
        @user = User.find_by(Name:params[:user][:Name])
        if @user && @user.authenticate(params[:user][:password])
            session[:id] = @user[:id]
            render json: session, status: 200
        else
            render json: {errors: "Wrong username or password"}, status: 422
        end
    end

    def destroy
        session.delete :id
        render json: {message: "Log out success"} , status: 200
    end
        
    respond_to :json
    skip_before_action :verify_authenticity_token, only: :create
end