Rails.application.routes.draw do
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :user, only: [:show, :create, :update]
      resources :session, only: [:create, :delete]
      delete '/user/logout' => "session#destroy"
      post '/user/login' => "session#create"
    end
  end
end